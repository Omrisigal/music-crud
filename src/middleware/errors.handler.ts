import log from '@ajar/marker';
import express, {Response, Request, NextFunction} from 'express';
import { myError } from '../error_messages.js';
import fs from 'fs';
import { errors_log } from '../constans.js';
const { White, Reset, Red } = log.constants;
const { NODE_ENV } = process.env;

// export const error_handler =  (err, req, res, next) => {
//     log.error(err);
//     next(err)
// }

// export const error_handler2 =  (err: Error, req: Request, res:Response, next:NextFunction) => {
//     if(NODE_ENV !== 'production')res.status(500).json({status:err.message,stack:err.stack});
//     else res.status(500).json({status:'internal server error...'});
// }

export const not_found =  (req: Request, res: Response) => {
    log.info(`url: ${White}${req.url}${Reset}${Red} not found...`);
    res.status(404).json({status:`url: ${req.url} not found...`});
}

// export async function errorHandler2 (err: Error, req: Request ,res: Response,next : NextFunction) {
//     res.status(403).send(req.url+" url not found ");
//   }

export async function errorHandler (err: myError, req : Request,res: Response,next: NextFunction) {
const stream = fs.createWriteStream('src/output/'+errors_log, {encoding:'utf-8', flags:"a"});
const status = err.status || 500;
const line = 'request id: '+req.id+ ' , error status: '+status+' , error message: ' + err.message + ' '+ ', stacktrace :'+ err.stack+ '\n';
fs.appendFile('src/output/'+ errors_log, line, function (error) {
    if (error) throw error;
    });
console.log('this is the error IN ERROR HANDLER: ',err.stack);
res.status(status).send(err.message);
}