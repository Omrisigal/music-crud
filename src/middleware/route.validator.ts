/* eslint-disable no-case-declarations */
import Joi from "joi";
import  {Response, Request, NextFunction} from 'express';

interface Rules{
    [key: string]: Joi.Schema;//indexer
}
function rulebuilder(): Rules{
    const first_name = Joi.string().alphanum().min(3).max(30);
    const last_name = Joi.string().alphanum().min(3).max(30);
    const email = Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } });
    return {first_name:first_name, last_name:last_name, email:email};
}

  export function generalValidator(schemaName: string) {
    return async function validator (req: Request ,res: Response, next: NextFunction) {
        const first_name = req.body.first_name;
        const last_name = req.body.last_name;
        const email = req.body.email;
        const rules =  rulebuilder();
        try{
            switch (schemaName){
                case 'CREATE':
                    for (const key in rules) {
                        rules[key] = rules[key].required();
                    }
                    const create_schema = Joi.object().keys(rules);
                    await create_schema.validateAsync({first_name,last_name,email});
                    break;
                case 'UPDATE':
                    const update_schema = Joi.object().keys(rules);
                    await update_schema.validateAsync({first_name,last_name,email});
                    break;
            }
            next();
        }
        catch(err){
            next(err);
        }
      }
  }