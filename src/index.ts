/* eslint-disable @typescript-eslint/no-explicit-any */
// require('dotenv').config();
import express from 'express'
import morgan from 'morgan'
import log from '@ajar/marker'
import cors from 'cors'

import {connect_db} from './db/mongoose.connection.js';
import user_router from './modules/user/user.router.js';
import song_router from './modules/song/song.router.js';
import artist_router from './modules/artist/artist.router.js';
import playlist_router from './modules/playlist/playlist.router.js';


import {errorHandler,not_found} from './middleware/errors.handler.js';   


class App {
  app: any;
  
  constructor() {
    
    this.app = express();
    this.applyGlobalMiddleware()
    this.applyRouters()
    this.applyErrorHandlers()
    this.app.use('*', not_found)
    this.startServer()

  }
  applyGlobalMiddleware(){
     // middleware
    this.app.use(cors());
    this.app.use(morgan('dev'))
  }
  applyRouters(){
    this.app.use('/api/users', user_router);
    this.app.use('/api/songs', song_router);
    this.app.use('/api/playlists', playlist_router);
    this.app.use('/api/artists', artist_router);

    
  }
  applyErrorHandlers(){
    this.app.use(errorHandler);    
  }
  async startServer() {
    const { PORT=8080 ,HOST="localhost", DB_URI = "mongodb://localhost:27017/crud-demo" } = process.env;
    await connect_db(DB_URI as string);  
    await this.app.listen(PORT,HOST);
    log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
  }
}

const myServerInstance = new App();


