import user_model,{user} from "../modules/user/user.model.js";
import song_model, { ISong } from "../modules/song/song.model.js";
import artist_model from "../modules/artist/artist.model.js";
import playlist_model from "../modules/playlist/playlist.model.js";
import songModel, { SongSchema } from "../modules/song/song.model.js";
import { ObjectId, Schema } from "mongoose";
import PLAYLIST_DAL from "./mongo_playlist_dal.js";
import ARTIST_DAL from "./mongo_artist_dal.js";
import { ICreateSongDto } from "../modules/song/song.interface.js";


class SONG_DAL{
        
        async getAllSongs(){
            return await song_model.find();
        }
        async getSongByID(id: string) {
            return await song_model.findById(id);
        }
        async deleteByID(id: string){
            return await song_model.findByIdAndDelete(id);
        }

        async create(data:ICreateSongDto){
            const song = await song_model.create(data);
            const artist = await ARTIST_DAL.getArtistByID(data.artist);
            // const artist = await artist_model.findById(data.artist);
            artist.songs.push(song._id);
            await artist.save();
            return song;
        }

        async addPlaylistToSong(song_id: string, playlist_id: string){
            const song = await this.getSongByID(song_id);
            const playlist = await PLAYLIST_DAL.getPlaylistByID(playlist_id);
            if(song){
                song.playlists.push(playlist);
                await song.save();
            }
            const added_playlist = await PLAYLIST_DAL.addSongToPlaylist(playlist_id, song_id);
            return song;
        }
       
}
export default new SONG_DAL();

