import mongoose from 'mongoose'
import log      from '@ajar/marker'

export const connect_db = async (uri:string) => {
    console.log("    URI    ",uri);
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true
    };
    await mongoose.connect(uri);
    log.magenta(' ✨  Connected to Mongo DB ✨ ')
}
