import user_model,{user} from "../modules/user/user.model.js";
import song_model from "../modules/song/song.model.js";
import artist_model from "../modules/artist/artist.model.js";
import playlist_model from "../modules/playlist/playlist.model.js";
import { SongSchema } from "../modules/song/song.model.js";
import { ObjectId, Schema } from "mongoose";
import { ICreateArtistDto } from "../modules/artist/artist.interface.js";

class ARTIST_DAL{
        
        async getAllArtists(){
            return await artist_model.find();
        }
        async getArtistByID(id: string ) {
            return await artist_model.findById(id);
        }

        async deleteByID(id: string){
            return await artist_model.findByIdAndDelete(id);
        }
        async create(data:ICreateArtistDto){
            return await artist_model.create(data);
        }
        async getArtistSongsByID(id:string) {
            return await artist_model.findById(id).populate('songs');

        }
        
        
}
export default new ARTIST_DAL();

