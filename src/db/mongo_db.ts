
import user_model,{user} from "../modules/user/user.model.js";
import song_model from "../modules/song/song.model.js";
import artist_model from "../modules/artist/artist.model.js";
import playlist_model from "../modules/playlist/playlist.model.js";


// export async function removeArtist(artist_id: string) {
//     const artist = await artist_model.findByIdAndDelete(artist_id);
//     return artist;
// }

// export async function getAllPlaylists(){
//     const playlists = await playlist_model.find();
//     return playlists;
// }
// export async function getPlaylistByID(id:string){
//     const playlist = await playlist_model.findById(id);
//     return playlist;
// }
// export async function removeFromPlaylist(playlist_id:string, song_id :string){
//     const song = await song_model.findById(song_id);
//     const playlist_index = song.playlists.findIndex((playlist:any)=>playlist._id === playlist_id);
//     song.playlists.splice(playlist_index,1);
//     await song.save();
//     const playlist = await playlist_model.findById(playlist_id);
//     const song_index = playlist.songs.findIndex((song:any)=>song._id === song_id);
//     playlist.songs.splice(song_index,1);
//     await playlist.save();
//     return playlist;

// }

// export async function getArtistByID(id:string){
//     const artist = await artist_model.findById(id);
//     return artist;
// }

// export async function getAllSongsOfArtist(id:string){
//     const artist = await artist_model.findById(id).populate('songs');
//     return artist;
// }
// export async function getAllArtists(){
//     const artists = await artist_model.find();
//     return artists;
// }
// export async function getAllSongs(){
//     const songs = await song_model.find();
//     return songs;
// }
// export async function getSongByID(id:string){
//     const song = await song_model.findById(id);
//     return song;

// }
// export async function removeSong(song_id:string){
//     const song = await song_model.findByIdAndDelete(song_id);
//     return song;

// }

// export async function addToPlaylist(song_id:string, playlist_id: string){
//     const song = await song_model.findById(song_id);
//     song.playlists.push(playlist_id);
//     await song.save();
//     const playlist = await playlist_model.findById(playlist_id);
//     playlist.songs.push(song_id);
//     await playlist.save();
// }

// export async function createPlaylist(data : any) {
//     const result = await playlist_model.create(data);
//     if('songs' in data && data.songs.length>0){
//         const pending = data.songs.map(async (song_id: any) => {
//             const song = await song_model.findById(song_id);
//             song.playlists.push(result);
//             await song.save();
//         } )
//         await Promise.all(pending);
//     }
//     return result;
// }
export async function createUser(data : user) {
    const result = await user_model.create(data);
    return result;
}

// export async function addSong(data : any) {
//     const result = await song_model.create(data);
//     const artist = await artist_model.findById(data.artist);
//     artist.songs.push(result._id);
//     await artist.save();
//     return result;
// }

// export async function addArtist(data : any) {
//     const result = await artist_model.create(data);
//     return result;
// }


export async function getAllUsers(selectors:string[] = [], skip = 0, limit = 0) {
    const selectors_string = selectors.length>0? selectors.join(" ") : ""
    skip = skip? skip: 0;
    limit = limit? limit: 0;
    const users = await user_model.find()
                            .select(selectors_string)
                            .skip(skip).limit(limit);
    return users;
}

export async function getUserByID(id: string) {
    const user = await user_model.findById(id)
    return user;
}

export async function deleteUserByID( id: string){
    const user = await user_model.findByIdAndRemove(id);
    return user;
}


export async function updateUser(id: string , data: user){
    const user = await user_model.findByIdAndUpdate(id, data,  {new: true, upsert: false });
    return user;
}
