import * as DB from "../../db/mongo_db.js";
import  {Response, Request, NextFunction} from 'express';
import { UrlError } from "../../error_messages.js";
import * as playlist_service from "./playlist.service.js";



export async function createPlayList(req: Request, res: Response) {
    const playlist = await playlist_service.createPlaylist(req.body);
    // const playlist = await DB.createPlaylist(req.body);
    res.status(200).json(playlist);
}

export async function getAllPlaylists(req: Request, res: Response) {
    const playlists = await playlist_service.getAllPlaylists();
    res.status(200).json(playlists);
}

export async function getPlaylistByID(req: Request, res: Response){
    const songs = await playlist_service.getPlaylistByID(req.params.id);
    if (!songs) {
        throw new UrlError('Url not found for request: '+req.baseUrl+req.url);
    }
    //return res.status(404).json({ status: "No user found." });
    res.status(200).json(songs);
};

export async function removeFromPlaylist(req: Request, res: Response){
    const playlist = await playlist_service.removeFromPlaylist(req.body.playlist_id, req.body.song_id);
    res.status(200).json(playlist);
}


