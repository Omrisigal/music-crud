/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import {getAllPlaylists,getPlaylistByID,removeFromPlaylist, createPlayList} from "./playlist.controller.js";
  import express, {Request,Response, NextFunction } from 'express';
  
  const router = express.Router();

  
  // parse json req.body on post routes
  router.use(express.json())
  router.use((req:Request, res:Response, next:NextFunction)=>{
    req.id = Math.random().toString(36).substring(7);
    next();
  })
 
// CREATES A NEW PLAYLIST - POST METHOD
  router.post("/", raw(createPlayList));

  // GET ALL PLAYLISTS
  router.get("/", raw(getAllPlaylists));
  
  // GETS A SINGLE PLAYLIST
  router.get("/:id", raw(getPlaylistByID));

  
  // REMOVE SONG FROM PLAYLIST
  router.put("/remove/",raw(removeFromPlaylist));
  
  export default router;
  