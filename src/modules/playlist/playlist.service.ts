import SONG_DAL from "../../db/mongo_song_dal.js";
import ARTIST_DAL from "../../db/mongo_artist_dal.js";
import PLAYLIST_DAL from "../../db/mongo_playlist_dal.js";
import { SongSchema } from "../song/song.model.js";
import { IPlaylist } from "./playlist.model.js";
import { ICreatePlaylistDto } from "./playlist.interface.js";
import { ObjectId } from "mongoose";

export async function getAllPlaylists(){
    const playlists = await PLAYLIST_DAL.getAllplaylists();
    return playlists;
}
export async function getPlaylistByID(id:string){
    const playlist = await PLAYLIST_DAL.getPlaylistByID(id);
    return playlist;
}
export async function removeFromPlaylist(playlist_id:string, song_id :string){
    const playlist = await PLAYLIST_DAL.removeFromPlaylist(playlist_id,song_id);
    return playlist;

}

export async function createPlaylist(data : ICreatePlaylistDto) {
    const result = await PLAYLIST_DAL.create(data);
    return result;

}