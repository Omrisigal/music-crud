import mongoose,{ HydratedDocument } from 'mongoose'
import { SongSchema, ISong } from '../song/song.model.js';
const { Schema, model } = mongoose

export interface IPlaylist {
    name: string,
    genre: string,
    // user: IUser,
    songs: HydratedDocument<ISong>[];
  }
  

export const PlaylistSchema = new Schema({
    name  : { type : String, required : true },
    genre : { type : String, required : true },
    // songs : [{ type: Schema.Types.ObjectId, ref:'song'}]
    // user: { type: Schema.Types.ObjectId, ref:'user'},
    songs : [SongSchema]
}, {timestamps:true});
  
export default model('playlist',PlaylistSchema);