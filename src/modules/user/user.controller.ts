
import * as DB from "../../db/mongo_db.js";
import  {Response, Request, NextFunction} from 'express';
import { UrlError } from "../../error_messages.js";

// import user_model from "../user/user.model.mjs";
    
export async function createUser (req: Request, res: Response){
    const user = await DB.createUser(req.body);
    res.status(200).json(user);
}

export async function getAllUsers(req: Request, res: Response) {
    const users = await DB.getAllUsers(["_id" ,"first_name" ,"last_name" ,"email"]);
    res.status(200).json(users);

}

export async function getUserByID(req: Request, res: Response) {
    const user = await DB.getUserByID(req.params.id);
    console.log("NOT SUPOSED TO HGET HERE ",user);
    if (!user) {
        throw new UrlError('Url not found for request: '+req.baseUrl+req.url);
    }
    //return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
}

export async function deleteUser(req: Request, res: Response){
    const user = await DB.deleteUserByID(req.params.id);
    if (!user) return res.status(404).json({ status: "No user found." });
    res.status(200).json(user);
}

export async function updateUser(req: Request, res: Response){
    const user = await DB.updateUser(req.params.id,req.body);
    res.status(200).json(user);
}

export async function paginate(req: Request, res: Response){
    const users = await DB.getAllUsers( ["_id" ,"first_name" ,"last_name" ,"email"], 
    (parseInt(req.params.page) * parseInt(req.params.batch_size)), 
    parseInt(req.params.batch_size));
    res.status(200).json(users);
}






