import mongoose, { Types } from 'mongoose';
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name  : { type : String},
    last_name   : { type : String},
    email       : { type : String},
    phone       : { type : String},
    // playlist    : { type: Schema.Types.ObjectId, ref:'user'}

});
  
export default model('user',UserSchema);

export interface user {
    first_name  : string,
    last_name   : string,
    email       : string,
    phone       : string,
    // playlist    : Types.ObjectId
}