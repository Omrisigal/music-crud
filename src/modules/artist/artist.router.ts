/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import {getAllArtists, getAllSongsOfArtist, getArtistByID, addArtist, deleteArtist} from "./artist.controller.js";
  import express, {Request,Response, NextFunction } from 'express';
  
  const router = express.Router();

  
  // parse json req.body on post routes
  router.use(express.json())
  router.use((req:Request, res:Response, next:NextFunction)=>{
    req.id = Math.random().toString(36).substring(7);
    next();
  })
  // CREATES A NEW ARTIST - POST METHOD
  router.post("/", raw(addArtist));
  
  // GET ALL SONGS OF ARTIST
  router.get("/:id/songs", raw(getAllSongsOfArtist));
  
  // GETS A SINGLE ARTIST
  router.get("/:id", raw(getArtistByID));

  // GETS ALL ARTISTS
  router.get("/", raw(getAllArtists));

  router.delete("/:id",raw(deleteArtist));
      
    
    

  export default router;
  