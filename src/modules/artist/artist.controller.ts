import * as DB from "../../db/mongo_db.js";
import  {Response, Request, NextFunction} from 'express';
import { UrlError } from "../../error_messages.js";
import * as artist_service from "./artist.service.js";

export async function deleteArtist(req:Request, res:Response){
    const artist = await artist_service.removeArtist(req.params.id);
    res.status(200).json(artist);
}


export async function addArtist (req: Request, res: Response){
     const artist = await artist_service.addArtist(req.body);
     res.status(200).json(artist);
}

export async function getAllArtists(req: Request, res: Response){
    const users = await artist_service.getAllArtists();
    res.status(200).json(users);
};

export async function getArtistByID(req: Request, res: Response){
    const artist = await artist_service.getArtistByID(req.params.id);
    if (!artist) {
        throw new UrlError('Url not found for request: '+req.baseUrl+req.url);
    }
    //return res.status(404).json({ status: "No user found." });
    res.status(200).json(artist);
};

export async function getAllSongsOfArtist(req: Request, res: Response){
    const songs = await artist_service.getAllSongsOfArtist(req.params.id);
    res.status(200).json(songs);
};


