import mongoose, { Types } from 'mongoose'
import { SongSchema } from '../song/song.model.js';
const { Schema, model } = mongoose

export interface IArtist {
    first_name:string,
    last_name: string,
    songs: Types.ObjectId[],

}

export const ArtistSchema = new Schema({
    first_name  : { type : String, required : true },
    last_name   : { type : String, required : true },
    songs       : [ { type: Schema.Types.ObjectId, ref:'song'} ]
    // songs : [SongSchema]
}, {timestamps:true});
  
export default model('artist',ArtistSchema);