/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import {addSong,getAllSongs,getSongByID,removeSong, addToPlaylist} from "./song.controller.js";
  import express, {Request,Response, NextFunction } from 'express';
  
  const router = express.Router();

  
  // parse json req.body on post routes
  router.use(express.json())
  router.use((req:Request, res:Response, next:NextFunction)=>{
    req.id = Math.random().toString(36).substring(7);
    next();
  })
  // CREATES A NEW SONG - POST METHOD
  router.post("/", raw(addSong));
  
  // GET ALL SONGS
  router.get("/", raw(getAllSongs));
  
  // GETS A SINGLE SONG
  router.get("/:id", raw(getSongByID));

  // DELETES A SONG
  router.delete("/:id", raw(removeSong));
  
  // ADD SONG TO PLAYLIST
  router.post("/addtoplaylist/",raw(addToPlaylist));
  
  export default router;
  