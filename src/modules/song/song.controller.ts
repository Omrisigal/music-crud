// import * as DB from "../../db/mongo_db.js";
import * as song_service from "./song.service.js";

import  {Response, Request, NextFunction} from 'express';
import { UrlError } from "../../error_messages.js";

export async function addSong (req: Request, res: Response){
    const user = await song_service.addSong(req.body);
    res.status(200).json(user);
}

export async function getAllSongs(req: Request, res: Response) {
    const songs = await song_service.getAllSongs();
    res.status(200).json(songs);
}

export async function getSongByID(req: Request, res: Response){
    const song = await song_service.getSongByID(req.params.id);
    if (!song) {
        throw new UrlError('Url not found for request: '+req.baseUrl+req.url);
    }
    //return res.status(404).json({ status: "No user found." });
    res.status(200).json(song);
};

export async function removeSong(req: Request, res: Response){
    const song = await song_service.removeSong(req.params.id);
    res.status(200).json(song);
}
export async function addToPlaylist(req: Request, res: Response){
    const song = await song_service.addToPlaylist(req.body.song_id, req.body.playlist_id);
    res.status(200).json(song);
};


// export async function addSong (req: Request, res: Response){
//     const user = await DB.addSong(req.body);
//     res.status(200).json(user);
// }

// export async function getAllSongs(req: Request, res: Response) {
//     const songs = await DB.getAllSongs();
//     res.status(200).json(songs);
// }

// export async function getSongByID(req: Request, res: Response){
//     const song = await DB.getSongByID(req.params.id);
//     if (!song) {
//         throw new UrlError('Url not found for request: '+req.baseUrl+req.url);
//     }
//     //return res.status(404).json({ status: "No user found." });
//     res.status(200).json(song);
// };

// export async function removeSong(req: Request, res: Response){
//     const song = await DB.removeSong(req.params.id);
//     res.status(200).json(song);
// }
// export async function addToPlaylist(req: Request, res: Response){
//     const song = await DB.addToPlaylist(req.body.song_id, req.body.playlist_id);
//     res.status(200).json(song);
// };

